package com.faz.customcalendar;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MainDbHelper extends SQLiteOpenHelper {
    public static final String DB_NAME = "MAIN.DB";
    public static final int DB_VERSION = 1;

    private Context context;
    private MainDbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.context = context;
    }

    public Context getContext() {
        return context;
    }
    private static MainDbHelper instance;
    public static synchronized MainDbHelper getInstance(Context context)
    {
        if (instance == null)
            instance = new MainDbHelper(context);

        return instance;
    }

    SQLiteDatabase openedDatabase;

    @Override
    public SQLiteDatabase getWritableDatabase() {
        if( openedDatabase == null || !openedDatabase.isOpen() ) {
            openedDatabase = super.getWritableDatabase();
        }
        return openedDatabase;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DayNote.CREATE_TABLE_QUERY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DayNote.TABLE_NAME);
        onCreate(db);
    }

    public void recreateAll() {
        this.onUpgrade(this.getWritableDatabase(), 1, 1);
    }
}