package com.faz.customcalendar;

import android.content.ContentValues;
import android.database.Cursor;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by FAZ on 25.06.2017.
 */
public class DayNote {
    private static Set<String> dayHasNoteSet;

    public static void refreshSet() {
        dayHasNoteSet = new HashSet<>();
        ArrayList<DayNote> dayNotes = getAll(MApplication.getInstance().getMainDbHelper());
        for( DayNote dayNote : dayNotes ) {
            dayHasNoteSet.add(dayNote.day);
        }
    }

    public static Set<String> dayHasNoteSet() {
        if( dayHasNoteSet == null ) {
            refreshSet();
        }
        return dayHasNoteSet;
    }

    public static final String TABLE_NAME = "day_note";

    //------ SQL - запросы --------

    // Запрос создания таблицы
    public static final String CREATE_TABLE_QUERY =
            "CREATE TABLE "+TABLE_NAME+" ("
                    + "_ID INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + "day TEXT DEFAULT \"2017-06-10\","
                    + "note TEXT DEFAULT \"\""
                    + ");";
    //---- / SQL - запросы---------


    // ---- Поля сущности ----
    public long _ID                  = -1;
    public String note               = "";
    public String day                = "2017-06-01";
    // --- / Поля сущности ---

    public DayNote(){}

    public boolean save() {
        return save(MApplication.getInstance().getMainDbHelper());
    }

    public boolean save(MainDbHelper dbHelper){
        ContentValues cv = new ContentValues();
        cv.put("note", this.note);
        cv.put("day", this.day);
        this.delete(dbHelper);
        return insert(dbHelper, cv);
    }

    private boolean insert(MainDbHelper dbHelper, ContentValues cv) {
        boolean insertResult = dbHelper.getWritableDatabase().insert(TABLE_NAME, null, cv)!=-1;
        return insertResult;
    }

    public boolean delete() {
        return delete(MApplication.getInstance().getMainDbHelper());
    }

    public boolean delete(MainDbHelper dbHelper) {
        return dbHelper.getWritableDatabase().delete(TABLE_NAME, "day='"+this.day+"'", null) > 0;
    }

    public static void deleteAll(MainDbHelper dbHelper) {
        dbHelper.getWritableDatabase().execSQL("DELETE FROM "+TABLE_NAME+" WHERE 1=1");
    }

    public static DayNote getByDay(String day) {
        return getByDay(MApplication.getInstance().getMainDbHelper(), day);
    }

    /**
     * @param dbHelper
     * @param day 2017-02-01
     * @return
     */
    public static DayNote getByDay(MainDbHelper dbHelper, String day) {
        ArrayList<DayNote> dayNotes = getAllByCondition(dbHelper, "day='"+day+"'");
        if( !dayNotes.isEmpty()) {
            return dayNotes.get(0);
        }
        return null;
    }

    public static ArrayList<DayNote> getAll(MainDbHelper dbHelper) {
        return getAllByCondition(dbHelper, "1=1");
    }

    public static ArrayList<DayNote> getAllByCondition(MainDbHelper dbHelper, String condition) {
        ArrayList<DayNote> result = new ArrayList<>();
        Cursor c = readAllByCondition(dbHelper, condition);
        c.moveToFirst();

        for( int i=0; i<c.getCount(); i++ ) {
            DayNote dayNote = new DayNote();
            dayNote.setFromCursorRow(c);
            result.add(dayNote);
            c.moveToNext();
        }
        c.close();
        return result;
    }

    public static Cursor readAllByCondition(MainDbHelper dbHelper, String condition) {
        return dbHelper.getWritableDatabase().rawQuery(
                "SELECT * FROM "+TABLE_NAME+" WHERE "+condition
                ,null
        );
    }

    /**
     * Пересоздает таблицу сущностей<br/>
     * (Создает, если таблицы нет)
     */
    public static void recreateTable(MainDbHelper dbHelper) {
        dbHelper.getWritableDatabase().execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        dbHelper.getWritableDatabase().execSQL(CREATE_TABLE_QUERY);
    }

    public DayNote setFromCursorRow(Cursor c) {
        return setFromSource(c, CursorDataConverter.getInstance());
    }

    public DayNote setFromSource(Object source, TypeDataConverter converter) {
        this._ID        = converter.getLong(source, "_ID", this._ID);
        this.note       = converter.getString(source, "note", this.day);
        this.day        = converter.getString(source, "day", this.day);
        return this;
    }
}
