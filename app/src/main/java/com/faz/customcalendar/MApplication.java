package com.faz.customcalendar;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import com.facebook.stetho.Stetho;
public class MApplication extends Application {
    private static MApplication application;

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;
        initStetho();
    }

    private void initStetho(){
        // Create an InitializerBuilder
        Stetho.InitializerBuilder initializerBuilder =
                Stetho.newInitializerBuilder(this);

        // Enable Chrome DevTools
        initializerBuilder.enableWebKitInspector(
                Stetho.defaultInspectorModulesProvider(this)
        );

        // Enable command line interface
        initializerBuilder.enableDumpapp(
                Stetho.defaultDumperPluginsProvider(this)
        );

        // Use the InitializerBuilder to generate an Initializer
        Stetho.Initializer initializer = initializerBuilder.build();

        // Initialize Stetho with the Initializer
        Stetho.initialize(initializer);
    }

    public static MApplication getInstance() {
        return application;
    }

    public MainDbHelper getMainDbHelper() {
        return MainDbHelper.getInstance(getApplicationContext());
    }

}