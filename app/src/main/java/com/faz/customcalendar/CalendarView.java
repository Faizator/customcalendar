package com.faz.customcalendar;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.LinearLayout;
import org.joda.time.DateTime;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;


public class CalendarView extends LinearLayout
{
    private static final String LOGTAG = "Calendar View";

    // Сколько дней показывать
    private static final int DAYS_COUNT = 42;

    private static final String DATE_FORMAT = "MMM yyyy";

    private String dateFormat = DATE_FORMAT;


    private DateTime mDate = new DateTime().withDayOfMonth(1).withTime(0,0,0,0); // первый день отображаемого месяца

    private final DateTime mCurrentDate = new DateTime().withTime(0,0,0,0); // текущая дата


    private LinearLayout header;
    private GridView grid;


    public CalendarView(Context context)
    {
        super(context);
        initControl(context);
    }

    public CalendarView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        initControl(context);
    }

    public CalendarView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        initControl(context);
    }

    private void initControl(Context context)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.planevents_calendar_view, this);

        assignUiElements();

        updateCalendar();
    }

    public void selectMonth(DateTime dayInMonth) {
        this.mDate = dayInMonth.withDayOfMonth(1).withTime(0,0,0,0);
        updateCalendar();
    }

    /**
     *
     * @param year
     * @param month 1...12
     */
    public void selectMonth(int year, int month) {
        this.selectMonth( new DateTime().withYear(year).withMonthOfYear(month) );
    }

    private void assignUiElements()
    {
        header = (LinearLayout)findViewById(R.id.calendar_header);
        grid = (GridView)findViewById(R.id.calendar_grid);
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DayInfo dayInfo = (DayInfo) parent.getItemAtPosition(position);
                System.out.println(dayInfo);
                System.out.println(dayInfo.hasNote());
                System.out.println(dayInfo.isCurrentDate());
                String dayString = DateTimeHelper.dayStringFromDateTime(dayInfo.getDate());
                Intent intent = new Intent(getContext(), DayNoteEditActivity.class);
                intent.putExtra(DayNoteEditActivity.EXTRA_DAY, dayString);
                getContext().startActivity(intent);
            }
        });
        Log.d("TAG", "ASSIGNING UI ELEMENTS");
        Log.d("TAG", "grid is " + grid);
    }

    public void updateCalendar()
    {
        updateCalendar(null);
    }
//                                        2016-01-02
//                                         /
    public void updateCalendar(Set<String> dayHasNoteSet)
    {
        ArrayList<DayInfo> cells = new ArrayList<>();

        int monthBeginningCell = mDate.getDayOfWeek() - 1;

        // move calendar backwards to the beginning of the week
        DateTime temp = mDate.minusDays(monthBeginningCell);

        while (cells.size() < DAYS_COUNT)
        {
            DayInfo dayInfo = new DayInfo();
            dayInfo.setDate(temp);

            String dayString = dayInfo.getDayString();
            if( (dayHasNoteSet != null ) && dayHasNoteSet.contains(dayString) ) {
                dayInfo.setHasNote(true);
//                Log.d("TAG", "SWAP FOR " + dayString + ": " + dayInfoData.getPlusFact()+":"+dayInfoData.getMinusFact());
            } else {
                dayInfo.setHasNote(false);
            }

            if( temp.getMillis() == mCurrentDate.getMillis() ) {
                dayInfo.setIsCurrentDate(true);
            }
            if( (temp.getYear() == mDate.getYear()) && (temp.getMonthOfYear() == mDate.getMonthOfYear() ) ) {
                dayInfo.setIsCurrentMonth(true);
            }
            cells.add(dayInfo);
            temp = temp.plusDays(1);
        }

        grid.setAdapter(new CalendarAdapter(getContext(), cells));

        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);

    }

    private class CalendarAdapter extends ArrayAdapter<DayInfo>
    {
        private LayoutInflater inflater;

        public CalendarAdapter(Context context, ArrayList<DayInfo> days)
        {
            super(context, R.layout.planevents_calendar_day, days);
            inflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            DayInfo dayInfo = getItem(position);
            if (view == null) {
                view = new CalendarCellView(getContext());
            }
            CalendarCellView cellView = (CalendarCellView) view;
            cellView.setDayInfo(dayInfo);
            return cellView;
        }
    }
}