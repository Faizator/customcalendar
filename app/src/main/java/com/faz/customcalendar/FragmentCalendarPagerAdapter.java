package com.faz.customcalendar;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


public class FragmentCalendarPagerAdapter extends FragmentPagerAdapter {
    public static final int YEARS_RANGE = 10;
    int FIRST_YEAR;
    public static final int PAGE_COUNT = 12*YEARS_RANGE;

    /**
     * @param fm
     * @param context
     * @param firstYear
     */
    public FragmentCalendarPagerAdapter(FragmentManager fm, Context context, int firstYear) {
        super(fm);
        this.FIRST_YEAR = firstYear;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public CalendarPageFragment getItem(int position) {
        int month = position%12; // 0...11
        int addYear = position/12;
        CalendarPageFragment calendarPageFragment = CalendarPageFragment.newInstance(FIRST_YEAR+addYear, month+1);
        return calendarPageFragment;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}