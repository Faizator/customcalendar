package com.faz.customcalendar;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CalendarCellView extends LinearLayout
{

    private TextView dayView;
    private TextView bulletsView;
    private LinearLayout eventsView;
    private FrameLayout eventsViewHolder;
    private DayInfo dayInfo;
    private LinearLayout dayRootView;


    public final int COLOR_GREY = Color.parseColor("#F0F0F0");
    public final int COLOR_WHITE = Color.parseColor("#FFFFFF");
    private final int COLOR_GREEN = Color.parseColor("#388E3C");
    private final int COLOR_RED = Color.parseColor("#D32F2F");
    private final int TEXT_SIZE = 11;

    public CalendarCellView(Context context)
    {
        super(context);
        initControl(context);
    }

    public CalendarCellView(Context context, AttributeSet attrs)
    {
        this(context);
    }

    public CalendarCellView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        this(context);
    }

    /**
     * Load control xml layout
     */
    private void initControl(Context context)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.planevents_calendar_day, this);

        assignUiElements();
    }

    public void setDayInfo(DayInfo dayInfo) {
        this.dayInfo = dayInfo;

        updateCell();
    }

    public void updateCell() {
        bulletsView.setVisibility(GONE);
        if (dayInfo == null) {
            Log.w("TAG", "EMPTY DAYINFO");
            return;
        } else {
            if( dayInfo.hasNote() ) {
                bulletsView.setVisibility(VISIBLE);
            }
        }
        eventsView.removeAllViews();

        int backColor = COLOR_GREY;

        dayRootView.setBackgroundColor(COLOR_GREY);
        if (dayInfo.isCurrentMonth()) {
            dayRootView.setBackgroundColor(COLOR_WHITE);
            backColor = COLOR_WHITE;
        }
        bulletsView.setBackgroundColor(backColor);

        if (dayInfo.isCurrentDate()) {
            dayView.setTextColor(Color.WHITE);
            dayView.setBackgroundResource(R.drawable.select_circle);
        } else {
            dayView.setTextColor(Color.BLACK);
            dayView.setBackgroundResource(0);
        }

        dayView.setText(String.valueOf(this.dayInfo.getDate().getDayOfMonth()));
    }

    private void assignUiElements()
    {
        dayRootView = (LinearLayout) findViewById(R.id.dayRootView);
        dayView = (TextView)findViewById(R.id.dayView);
        eventsView = (LinearLayout)findViewById(R.id.eventsView);
        eventsViewHolder = (FrameLayout)findViewById(R.id.eventsViewHolder);
        bulletsView = (TextView)findViewById(R.id.bulletsView);

    }

}