package com.faz.customcalendar;

public interface OnAbsoluteMonthPickListener {
    /**
     *
     * @param month 201501 / 201612 etc...
     */
    void onAbsoluteMonthPick(long month);
}
