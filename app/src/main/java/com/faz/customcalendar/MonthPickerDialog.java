package com.faz.customcalendar;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class MonthPickerDialog extends DialogFragment {
    private OnAbsoluteMonthPickListener mOnAbsoluteMonthPickListener;

    private TextView mPickedYearTextView;
    private TextView mPickedMonthTextView;
    private ImageView mPickedMonthArrowView;
    private ImageView mPickedYearArrowView;


    private int mMinYear;
    private int mMaxYear;
    private int mPickedYear;
    private int mPickedMonth;
    private MyYearAdapter mYearAdapter;

    private int COLOR_WHITE= Color.parseColor("#FFFFFF");
    private int COLOR_WHITE_TRANS = Color.parseColor("#88FFFFFF");

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPickedMonth = getArguments().getInt("PICKED_MONTH");
        mPickedYear = getArguments().getInt("PICKED_YEAR");
        mMinYear = getArguments().getInt("MIN_YEAR");
        mMaxYear = getArguments().getInt("MAX_YEAR");
        mYearAdapter = new MyYearAdapter(getActivity(), R.layout.year_list_item, R.id.yearView, mMinYear, mMaxYear);
        mYearAdapter.setCheckedYear("" + mPickedYear);
    }


    private class MonthClickListener implements View.OnClickListener {
        private int monthNumber;
        public MonthClickListener(int i) {
            monthNumber = i;
        }
        @Override
        public void onClick(View v) {
            Log.d("TAG", "CLICKED MONTH " + monthNumber);
            pickMonth(monthNumber);
        }
    }

    final String LOG_TAG = "myLogs";
    private GridLayout monthsGrid;
    private ListView yearsList;


    private long getPickedAbsoluteMonth() {
        return DateTimeHelper.absoluteMonthFromYearAndMonth(this.getPickedYear(), this.getPickedMonth()+1);
    }

    private int getPickedYear() {
        return mYearAdapter.getCheckedYear();
    }
    private int getPickedMonth(){
        return mPickedMonth;
    }

    private void pickMonth(int month) { // 0...11
        mPickedMonth = month;
        int childCount = monthsGrid.getChildCount();
        for (int i= 0; i < childCount; i++){
            FrameLayout container = (FrameLayout) monthsGrid.getChildAt(i);
            TextView tw = (TextView) container.getChildAt(0);
            tw.setTextColor(Color.BLACK);
            tw.setBackgroundResource(0);
            if( i == mPickedMonth ) {
                tw.setTextColor(Color.WHITE);
                tw.setBackgroundResource(R.drawable.select_circle);
            }
        }
        mPickedMonthTextView.setText(DateTimeHelper.monthNameFromMonthNumber(month));
    }

    private void pickYear(int year) {
        mPickedYear = year;
        mPickedYearTextView.setText(""+year);
        mYearAdapter.setCheckedYear(""+year);
    }

    private void confirmChoice() {
        long absoluteMonth = getPickedAbsoluteMonth();//DateTimeHelper.absoluteMonthFromYearAndMonth(mPickedYear, mPickedMonth+1);
        Log.d("TAG", "CONFIRM " + absoluteMonth);
        if( mOnAbsoluteMonthPickListener != null ) {
            mOnAbsoluteMonthPickListener.onAbsoluteMonthPick(absoluteMonth);
        }
    }

    /**
     *
     * @param pickedMonth 0..11
     * @return
     */
    public static MonthPickerDialog newInstance(int minYear, int maxYear, int pickedYear, int pickedMonth) {
        if( pickedMonth < 0 || pickedMonth > 11 ) {
            throw new RuntimeException("PICKED MONTH MUST BE BETWEEN 0 AND 11");
        }
        MonthPickerDialog f = new MonthPickerDialog();
        Bundle args = new Bundle();
        args.putInt("PICKED_MONTH", pickedMonth);
        args.putInt("PICKED_YEAR", pickedYear);
        args.putInt("MIN_YEAR", minYear);
        args.putInt("MAX_YEAR", maxYear);
        f.setArguments(args);
        return f;
    }

    public void setOnAbsoluteMonthPickListener(OnAbsoluteMonthPickListener listener) {
        this.mOnAbsoluteMonthPickListener = listener;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup parent,
                             Bundle savedInstanceState) {

        this.getDialog().setCanceledOnTouchOutside(false);
        this.getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        View v = inflater.inflate(R.layout.month_picker_dialog, null);
        v.findViewById(R.id.cancelBtn).setOnClickListener(mCancelListener);
        v.findViewById(R.id.confirmBtn).setOnClickListener(mConfirmListener);

        mPickedMonthTextView = (TextView) v.findViewById(R.id.picked_month);
        mPickedMonthArrowView = (ImageView) v.findViewById(R.id.picked_month_arrow);
        mPickedYearTextView = (TextView) v.findViewById(R.id.picked_year);
        mPickedYearArrowView = (ImageView) v.findViewById(R.id.picked_year_arrow);
        mPickedMonthTextView.setOnClickListener(mSwitchToMonthPick);
        mPickedYearTextView.setOnClickListener(mSwitchToYearPick);


        monthsGrid = (GridLayout) v.findViewById(R.id.months_grid);
        int childCount = monthsGrid.getChildCount();

        for (int i= 0; i < childCount; i++){
            FrameLayout container = (FrameLayout) monthsGrid.getChildAt(i);
            container.setOnClickListener(new MonthClickListener(i));
        }

        yearsList = (ListView) v.findViewById(R.id.years_view);
        mYearAdapter.setOnYearSetListener(new MyYearAdapter.OnYearSetListener() {
            @Override
            public void onYearSet(String year) {
                mPickedYearTextView.setText(year);
            }
        });
        yearsList.setAdapter(mYearAdapter);

        pickMonth(mPickedMonth);
        pickYear(mPickedYear);
        return v;
    }

    View.OnClickListener mCancelListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            dismiss();
        }
    };

    View.OnClickListener mConfirmListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            confirmChoice();
            dismiss();
        }
    };

    View.OnClickListener mSwitchToMonthPick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switchToMonthPick();
        }
    };

    View.OnClickListener mSwitchToYearPick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switchToYearPick();
        }
    };

    private void switchToMonthPick() {
        Log.d("TAG", "TO MONTH PICK");
        monthsGrid.setVisibility(View.VISIBLE);
        mPickedMonthTextView.setTextColor(COLOR_WHITE);
        mPickedMonthArrowView.setVisibility(View.GONE);
        yearsList.setVisibility(View.GONE);
        mPickedYearTextView.setTextColor(COLOR_WHITE_TRANS);
        mPickedYearArrowView.setVisibility(View.VISIBLE);
    }

    private void switchToYearPick() {
        Log.d("TAG", "TO YEAR SWITCH");
        monthsGrid.setVisibility(View.GONE);
        mPickedMonthTextView.setTextColor(COLOR_WHITE_TRANS);
        mPickedMonthArrowView.setVisibility(View.VISIBLE);
        yearsList.setVisibility(View.VISIBLE);
        mPickedYearTextView.setTextColor(COLOR_WHITE);
        mPickedYearArrowView.setVisibility(View.GONE);
    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Log.d(LOG_TAG, "MonthPickerDialog: onDismiss");
    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        Log.d(LOG_TAG, "MonthPickerDialog: onCancel");
    }
}