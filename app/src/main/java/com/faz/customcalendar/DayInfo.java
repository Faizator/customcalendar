package com.faz.customcalendar;
import org.joda.time.DateTime;
import java.util.ArrayList;

public class DayInfo {
    private DateTime date;
    private boolean isCurrentDate = false;
    private boolean isCurrentMonth = false;
    private boolean hasNote = false;
    private ArrayList<CalendarEventSimple> events = new ArrayList<>();
    public void setDate(DateTime date) {
        this.date = date;
    }

    public DateTime getDate() {
        return date;
    }

    public void setHasNote(boolean hasNote) {
        this.hasNote = hasNote;
    }

    public boolean hasNote() {
        return this.hasNote;
    }

    public void setIsCurrentDate(boolean isCurrentDate) {
        this.isCurrentDate = isCurrentDate;
    }
    public void setIsCurrentMonth(boolean isCurrentMonth) {
        this.isCurrentMonth = isCurrentMonth;
    }

    public boolean isCurrentDate() {
        return isCurrentDate;
    }

    public boolean isCurrentMonth() {
        return isCurrentMonth;
    }

    /**
     *
     * @return 2016-01-02 (format)
     */
    public String getDayString() {
        return DateTimeHelper.dayStringFromYMD(date.getYear(), date.getMonthOfYear(), date.getDayOfMonth());
    }
}