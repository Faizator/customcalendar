package com.faz.customcalendar;

public class CalendarEventSimple {
    private int color;
    private String description;
    private long eventTimeInSeconds;

    public CalendarEventSimple(String description, int color, long eventTimeInSeconds) {
        this.setDescription(description);
        this.setColor(color);
        this.eventTimeInSeconds = eventTimeInSeconds;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getColor() {
        return color;
    }

    public String getDescription() {
        return description;
    }
}
