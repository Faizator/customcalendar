package com.faz.customcalendar;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.ArrayList;

public class MyYearAdapter extends ArrayAdapter<Integer> {
    private String mCheckedYear;
    private int COLOR_GREEN = Color.parseColor("#7CB342");
    private int COLOR_BLACK = Color.parseColor("#000000");
    private OnYearSetListener onYearSetListener;

    public interface OnYearSetListener {
        void onYearSet(String year);
    }

    public MyYearAdapter(Context context, int resource, int textViewResourceId, int minYear, int maxYear) {
        super(context, resource, textViewResourceId);
        addAll(generateYears(minYear, maxYear));
    }


    public void setOnYearSetListener(OnYearSetListener listener) {
        this.onYearSetListener = listener;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View pv = super.getView(position, convertView, parent);
        TextView v = (TextView) pv.findViewById(R.id.yearView);
        if (v.getText().toString().equals(mCheckedYear) ) {
            v.setTextSize(22);
            v.setTextColor(COLOR_GREEN);
        } else {
            v.setTextSize(15);
            v.setTextColor(COLOR_BLACK);
        }
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCheckedYear(((TextView)v).getText().toString());
            }
        });
        return v;
    }

    public int getCheckedYear() {
        return Integer.parseInt(mCheckedYear);
    }

    public void setCheckedYear(String year) {
        mCheckedYear = year;
        Log.d("TAG", "SETTING CHECKED YEAR");
        if( onYearSetListener != null ) {
            onYearSetListener.onYearSet(year);
        }
        notifyDataSetChanged();
    }


    private ArrayList<Integer> generateYears(int min, int max) {
        ArrayList<Integer> list = new ArrayList<>();
        for( int i=min; i<=max; i++ ){
            list.add(i);
        }
        return list;
    }
}
