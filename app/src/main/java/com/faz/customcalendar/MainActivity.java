package com.faz.customcalendar;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import org.joda.time.DateTime;

public class MainActivity extends AppCompatActivity {
    ViewPager viewPager;
    FragmentCalendarPagerAdapter fragmentPagerAdapter;

    int FIRST_YEAR;
    int CENTRAL_YEAR;
    int currentSelectedYear;
    int currentSelectedMonth; //1...12
    final int YEARS_RANGE = FragmentCalendarPagerAdapter.YEARS_RANGE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DateTime now = new DateTime();
        CENTRAL_YEAR = now.getYear();
        currentSelectedYear = CENTRAL_YEAR;
        int CURRENT_MONTH = now.getMonthOfYear(); //1...12
        currentSelectedMonth = CURRENT_MONTH;
        FIRST_YEAR = CENTRAL_YEAR - YEARS_RANGE/2;
        fragmentPagerAdapter = new FragmentCalendarPagerAdapter(getSupportFragmentManager(), this, FIRST_YEAR);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(fragmentPagerAdapter);
        viewPager.addOnPageChangeListener(onPageChangeListener);
        viewPager.setCurrentItem(12*(YEARS_RANGE/2) + CURRENT_MONTH-1);
    }

    @Override
    protected void onResume() {
        super.onResume();
        fragmentPagerAdapter.notifyDataSetChanged();
    }

    private ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

        @Override
        public void onPageSelected(int position) {
            int monthNumber = position%12; // 0...11
            int year = CENTRAL_YEAR - YEARS_RANGE/2 + position/12;
            String monthName = DateTimeHelper.monthNameFromMonthNumber(monthNumber);
            setTitle(monthName + " " + year);
            currentSelectedMonth = monthNumber+1;
            Log.d("TAG", "MONTH " +monthName);
            currentSelectedYear = year;
        }

        @Override
        public void onPageScrollStateChanged(int state) {}
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_planning, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_pick_date:
                pickDate();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void pickDate() {
        Log.d("TAG", "PICKING DATE");
        Log.d("TAG", "PICKING DATE CUR YEAR " + currentSelectedYear);
        MonthPickerDialog monthPickerDialog = MonthPickerDialog.newInstance(
                CENTRAL_YEAR - YEARS_RANGE/2,
                CENTRAL_YEAR + YEARS_RANGE/2 - 1,
                currentSelectedYear,
                currentSelectedMonth-1);

        monthPickerDialog.setOnAbsoluteMonthPickListener(new OnAbsoluteMonthPickListener() {
            @Override
            public void onAbsoluteMonthPick(long month) {
                System.out.println("MONTH PICKED " + month);
                int pickedYear  = DateTimeHelper.monthYearFromAbsoluteMonth(month);
                int pickedMonth = DateTimeHelper.monthMonthFromAbsoluteMonth(month);
                viewPager.setCurrentItem(12*(pickedYear-FIRST_YEAR) + pickedMonth - 1 );
            }
        });
        monthPickerDialog.show(getFragmentManager(), "month_picker_dialog");
    }
}
