package com.faz.customcalendar;

import org.joda.time.DateTime;

/**
 * Created by FAZ on 25.06.2017.
 */

public class DateTimeHelper {
    static String[] MONTHS = {
            "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"
    };
    /**
     * @param monthNumber 0...11
     */
    public static String monthNameFromMonthNumber(int monthNumber) {
        return MONTHS[monthNumber];
    }

    /**
     *
     * @param absoluteMonth 201605
     * @return 2016
     */
    public static int monthYearFromAbsoluteMonth(long absoluteMonth) {
        return (int)absoluteMonth/100;
    }

    /**
     *
     * @param absoluteMonth 201605
     * @return 5
     */
    public static int monthMonthFromAbsoluteMonth(long absoluteMonth) {
        return (int)absoluteMonth%100;
    }

    /**
     *
     * @param year
     * @param month 1...12
     * @return
     */
    public static long absoluteMonthFromYearAndMonth(int year, int month) {
        String s = "" + year;
        if( month < 10) {
            s += "0";
        }
        s += month;
        return Long.valueOf(s);
    }

    /**
     *
     * @param year
     * @param month
     * @param day
     * @return 2016-01-12
     */
    public static String dayStringFromYMD(int year, int month, int day) {
        StringBuilder sb = new StringBuilder();
        sb.append(year);
        sb.append("-");
        if( month < 10 ) {
            sb.append(0);
        }
        sb.append(month);
        sb.append("-");
        if( day < 10 ) {
            sb.append(0);
        }
        sb.append(day);
        return sb.toString();
    }

    public static String dayStringFromDateTime(DateTime dt) {
        String month = ""+dt.getMonthOfYear();
        String day = ""+dt.getDayOfMonth();
        if( dt.getMonthOfYear() < 10 ) {
            month = "0" + month;
        }
        if( dt.getDayOfMonth() < 10 ) {
            day = "0" + day;
        }
        return dt.getYear() + "-" + month + "-" + day;
    }


}
