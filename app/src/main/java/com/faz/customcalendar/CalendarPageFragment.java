package com.faz.customcalendar;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

public class CalendarPageFragment extends Fragment {
    public static final String ARG_MONTH = "ARG_MONTH";
    public static final String ARG_YEAR = "ARG_YEAR";

    CalendarView calendarView;
    int year;
    int month;

    /**
     * @param year
     * @param month 1...12
     * @return
     */
    public static CalendarPageFragment newInstance(int year, int month){
        Bundle args = new Bundle();
        args.putInt(ARG_MONTH, month);
        args.putInt(ARG_YEAR, year);

        CalendarPageFragment fragment = new CalendarPageFragment();
        fragment.month = month;
        fragment.year = year;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if( (args!= null) && args.containsKey(ARG_MONTH) && args.containsKey(ARG_YEAR) ) {
            this.year = args.getInt(ARG_YEAR);
            this.month = args.getInt(ARG_MONTH);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.planevents_fragment_page, container, false);
        Log.d("TAG", "CREATED VIEW " + view);
        if( view instanceof FrameLayout ) {
            Log.d("TAG", "YES");
        } else {
            Log.d("TAG", "NO");
        }
        calendarView = (CalendarView) view.findViewById(R.id.cw);
        Log.d("TAG", "CAL VIEW " + calendarView);
        calendarView.selectMonth(year, month);
        refreshCalendar();
//        calendarView.updateCalendar(null);
        TextView tv = (TextView) view.findViewById(R.id.tw);
        Log.d("TAG", "CAL TW" + tv);
        return view;
    }

    public void refreshCalendar() {
        calendarView.updateCalendar( DayNote.dayHasNoteSet() );
    }
}
