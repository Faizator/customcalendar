package com.faz.customcalendar;

import android.app.Application;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DayNoteEditActivity extends AppCompatActivity {
    public final static String EXTRA_DAY = "EXTRA_DAY";

    @BindView(R.id.noteEdit) EditText noteEdit;
    @BindView(R.id.saveBtn) Button saveBtn;
    @BindView(R.id.deleteBtn) Button deleteBtn;

    String DAY;
    DayNote dayNote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_day_note_edit);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DAY = getIntent().getStringExtra(EXTRA_DAY);
        setTitle("Заметка на " + DAY);

        dayNote = DayNote.getByDay(DAY);

        if( dayNote != null ) {
            noteEdit.setText(dayNote.note);
        } else {
            dayNote = new DayNote();
            dayNote.day = DAY;
        }

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dayNote.note = noteEdit.getText().toString();
                dayNote.save();
                DayNote.refreshSet();
                finish();
            }
        });

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DayNote dayNote = DayNote.getByDay(DAY);
                if( dayNote != null ) {
                    dayNote.delete();
                }
                DayNote.refreshSet();
                finish();
            }
        });
    }
}
